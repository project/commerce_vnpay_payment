<?php

namespace Drupal\commerce_vnpay_payment\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_vnpay_payment\VNPayResponseCode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides VNPay gateway.
 *
 * @CommercePaymentGateway(
 *    id = "commerce_vnpay_payment",
 *    label = @Translation("VNPay"),
 *    display_label = @Translation("VNPay"),
 *    forms = {
 *      "offsite-payment" = "Drupal\commerce_vnpay_payment\PluginForm\OffsiteRedirect\VNPayPaymentForm",
 *    },
 *    requires_billing_information = FALSE,
 * )
 */
class VNPayOffsite extends OffsitePaymentGatewayBase implements VNPayOffsiteInterface {

  /**
   * Drupal\Core\Logger\LoggerChannel.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.channel.commerce_vnpay_payment');
    /** @var \Drupal\Core\Http\ClientFactory $http_client_factory */
    $instance->rounder = $container->get('commerce_price.rounder');
    $instance->messenger = $container->get('messenger');
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = [
      'vnp_Url' => 'https://sandbox.vnpayment.vn/paymentv2/vpcpay.html',
      'vnp_TmnCode' => '',
      'vnp_HashSecret' => '',
    ];
    return $configuration + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['vnp_Url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('vnp_Url'),
      '#default_value' => $this->configuration['vnp_Url'] ?? 'https://sandbox.vnpayment.vn/paymentv2/vpcpay.html',
      '#required' => TRUE,
    ];
    $form['vnp_TmnCode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('vnp_TmnCode'),
      '#default_value' => $this->configuration['vnp_TmnCode'] ?? '',
      '#required' => TRUE,
    ];
    $form['vnp_HashSecret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('vnp_HashSecret'),
      '#default_value' => $this->configuration['vnp_HashSecret'] ?? '',
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $timezone = date_default_timezone_get();
    if ($timezone != 'Asia/Ho_Chi_Minh') {
      $regional_settings_url = Url::fromRoute('system.regional_settings')->toString();
      $form_state->setError($form, t('VNPay only support region "Asia/Ho_Chi_Minh". <a href="@href" target="_blank">Link</a>.', ['@href' => $regional_settings_url]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['vnp_Url'] = $values['vnp_Url'];
      $this->configuration['vnp_TmnCode'] = $values['vnp_TmnCode'];
      $this->configuration['vnp_HashSecret'] = $values['vnp_HashSecret'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPayUrl(PaymentInterface $payment) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();
    $vnp_Url = $this->getInputDataAndVNPayUrl($order, $payment);
    return $vnp_Url;
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $query = $request->query->all();
    if (!isset($query['vnp_ResponseCode'])) {
      throw new PaymentGatewayException('No response from payment API.');
    }
    if ($query['vnp_ResponseCode'] != VNPayResponseCode::SUCCESS && $query['vnp_TransactionStatus'] != VNPayResponseCode::SUCCESS) {
      $message = VNPayResponseCode::MESSAGES[VNPayResponseCode::SUCCESS];
      if ($message) {
        $message = t($message);
      }
      else {
        $message = t('An error occurred during transaction');
      }
      $this->messenger->addError(t($message));
      throw new PaymentGatewayException(t($message));
    }

    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'authorization',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $query['vnp_TransactionNo'],
      'remote_state' => 'completed',
    ]);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger
      ->addError($this->t('You have canceled checkout at @gateway.', [
        '@gateway' => $this->getDisplayLabel(),
      ]), 'error');
  }

  /**
   * {@inheritdoc}
   */
  public function getReturnUrl(OrderInterface $order) {
    return Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderInfo(OrderInterface $order) {
    $order_info = ['Order Id: ' . $order->id()];
    foreach ($order->getItems() as $order_item) {
      $order_info[] = $order_item->label();
    }
    return implode(' _ ', $order_info);
  }

  /**
   * {@inheritdoc}
   */
  public function getInputDataAndVNPayUrl(OrderInterface $order, PaymentInterface $payment) {
    /** @var \Drupal\commerce_price\Price $amount */
    $amount = $this->rounder->round($payment->getAmount());
    $vnp_Url = $this->configuration['vnp_Url'];
    $vnp_Returnurl = $this->getReturnUrl($order)->toString();
    $vnp_TmnCode = $this->configuration['vnp_TmnCode'];
    $vnp_HashSecret = $this->configuration['vnp_HashSecret'];
    $vnp_OrderInfo = $this->getOrderInfo($order);
    $vnp_Amount = $amount->getNumber();
    $vnp_CurrCode = $amount->getCurrencyCode();
    $vnp_Locale = $this->languageManager->getCurrentLanguage()->getId();
    $vnp_IpAddr = $order->getIpAddress();
    // Add Params of 2.0.1 Version.
    $vnp_CreateDate = date('YmdHis');
    $vnp_ExpireDate = date('YmdHis', strtotime('+15 minutes', strtotime($vnp_CreateDate)));
    $vnp_TxnRef = sprintf('%s%s', $order->id(), $vnp_CreateDate);
    $vnp_Bill_Email = $order->getEmail();

    $inputData = [
      'vnp_Version' => '2.1.0',
      'vnp_TmnCode' => $vnp_TmnCode,
      'vnp_Amount' => $vnp_Amount * 100,
      'vnp_Command' => 'pay',
      'vnp_CreateDate' => $vnp_CreateDate,
      'vnp_CurrCode' => $vnp_CurrCode,
      'vnp_IpAddr' => $vnp_IpAddr,
      'vnp_Locale' => $vnp_Locale,
      'vnp_OrderInfo' => $vnp_OrderInfo,
      'vnp_OrderType' => 'other',
      'vnp_ReturnUrl' => $vnp_Returnurl,
      'vnp_TxnRef' => $vnp_TxnRef,
      'vnp_ExpireDate' => $vnp_ExpireDate,
      'vnp_Bill_Email' => $vnp_Bill_Email,
      'vnp_Inv_Email' => $vnp_Bill_Email,
    ];

    /** @var \Drupal\profile\Entity\Profile $billingProfile */
    $billingProfile = $order->getBillingProfile();
    if ($billingProfile->hasField('address') && !$billingProfile->get('address')->isEmpty()) {
      $address = $billingProfile->get('address')->first()->getValue();

      if (!empty($address['given_name'])) {
        $inputData['vnp_Bill_FirstName'] = $address['given_name'];
      }
      if (!empty($address['family_name'])) {
        $inputData['vnp_Bill_LastName'] = $address['family_name'];
      }
      if (!empty($address['address_line1'])) {
        $txt_inv_addr1 = $address['address_line1'];
        $inputData['vnp_Bill_Address'] = $txt_inv_addr1;
        $inputData['vnp_Inv_Address'] = $txt_inv_addr1;
      }
      if (!empty($address['locality'])) {
        $inputData['vnp_Bill_City'] = $address['locality'];
      }
      if (!empty($address['country_code'])) {
        $inputData['vnp_Bill_Country'] = $address['country_code'];
      }
    }

    ksort($inputData);
    $query = '';
    $i = 0;
    $hash_Data = '';
    foreach ($inputData as $key => $value) {
      if ($i == 1) {
        $hash_Data .= '&' . urlencode($key) . '=' . urlencode($value);
      }
      else {
        $hash_Data .= urlencode($key) . '=' . urlencode($value);
        $i = 1;
      }
      $query .= urlencode($key) . '=' . urlencode($value) . '&';
    }

    $vnp_Url = $vnp_Url . '?' . $query;
    if (isset($vnp_HashSecret)) {
      $vnp_SecureHash = hash_hmac('sha512', $hash_Data, $vnp_HashSecret);
      $vnp_Url .= 'vnp_SecureHash=' . $vnp_SecureHash;
    }
    return $vnp_Url;
  }

}
