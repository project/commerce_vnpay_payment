<?php

namespace Drupal\commerce_vnpay_payment;

/**
 *
 */
class VNPayResponseCode {

  const SUCCESS = '00';

  const TRANS_EXIST = '01';

  const INVALID_MERCHANT = '02';

  const INVALID_FORMAT = '03';

  const WEBSITE_LOCKED = '04';

  const FAIL_PASSWORD_TIMES = '05';

  const INVALID_OPT = '13';

  const SUSPECTED_FRAUD = '07';

  const FAIL_INTERNET_BANKING_REGISTRATION = '09';

  const CARD_VERIFY_FAIL = '10';

  const TRANS_EXPIRE = '11';

  const CARD_LOCKED = '12';

  const INSUFFICIENT_ACCOUNT_BALANCE = '51';

  const DAILY_TRANS_LIMIT = '65';

  const BANK_SYSTEM_MAINTAINING = '08';

  const UNKNOWN_ERROR = '99';

  const INVALID_SECURE_HASH = '97';

  const MESSAGES = [
    VNPayResponseCode::SUCCESS => 'Transaction successful',
    VNPayResponseCode::TRANS_EXIST => 'Transaction already exists',
    VNPayResponseCode::INVALID_MERCHANT => 'Invalid Merchant (please check vnp_TmnCode)',
    VNPayResponseCode::INVALID_FORMAT => 'Data sent is not in correct format',
    VNPayResponseCode::WEBSITE_LOCKED => 'Transaction initiation unsuccessful due to Website being temporarily locked',
    VNPayResponseCode::FAIL_PASSWORD_TIMES => 'Transaction unsuccessful due to: Customer entered wrong password too many times. Please retry the transaction.',
    VNPayResponseCode::INVALID_OPT => 'Transaction unsuccessful due to Customer entered wrong transaction authentication password (OTP). Please retry the transaction.',
    VNPayResponseCode::SUSPECTED_FRAUD => 'Transaction suspected to be fraudulent.',
    VNPayResponseCode::FAIL_INTERNET_BANKING_REGISTRATION => 'Transaction unsuccessful due to: Customer card account is not registered for Internet Banking service at the bank.',
    VNPayResponseCode::CARD_VERIFY_FAIL => 'Transaction unsuccessful due to: Customer failed to verify card account information more than 3 times.',
    VNPayResponseCode::TRANS_EXPIRE => 'Transaction unsuccessful due to: Payment waiting time has expired. Please retry the transaction.',
    VNPayResponseCode::CARD_LOCKED => 'Transaction unsuccessful due to: Customer card account is locked.',
    VNPayResponseCode::INSUFFICIENT_ACCOUNT_BALANCE => 'Transaction unsuccessful due to: Customer account balance is insufficient to perform the transaction.',
    VNPayResponseCode::DAILY_TRANS_LIMIT => 'Transaction unsuccessful due to: Customer account has exceeded the daily transaction limit.',
    VNPayResponseCode::BANK_SYSTEM_MAINTAINING => 'Transaction unsuccessful due to: Bank system is under maintenance. Please refrain from using this bank card account for transactions temporarily.',
    VNPayResponseCode::UNKNOWN_ERROR => 'Other errors (remaining errors, not listed in the error code list).',
    VNPayResponseCode::INVALID_SECURE_HASH => 'Invalid signature.',
  ];

}
