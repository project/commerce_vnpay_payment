<?php

namespace Drupal\commerce_vnpay_payment\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 *
 */
interface VNPayOffsiteInterface {

  /**
   * Get return page url.
   *
   * @return \Drupal\Core\Url
   */
  public function getReturnUrl(OrderInterface $order);

  /**
   * Create new payment and get payUrl from MoMo API.
   *
   * @return string|null
   */
  public function getPayUrl(PaymentInterface $payment);

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return string
   */
  public function getOrderInfo(OrderInterface $order);

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *
   * @return array
   */
  public function getInputDataAndVNPayUrl(OrderInterface $order, PaymentInterface $payment);

}
